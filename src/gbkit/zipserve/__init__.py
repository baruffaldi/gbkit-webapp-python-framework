#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# GBKit
# - WebApps Python Handy Framework
#
# Copyright 2012 GB Consulting
# Author: Filippo Baruffaldi <filippo@baruffaldi.info>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
"""
@author: Filippo Baruffaldi <filippo@baruffaldi.info>
@organization: GB Consulting
@project: GBKit
@license: Apache 2.0
"""
__author__ = "Filippo Baruffaldi <filippo@baruffaldi.info>"
__organization__ = "GB Consulting"
__project__ = "GBKit"
__license__ = "Apache 2.0"
__url__ = "http://www.gbc-italy.com"
__version__ = 1

import webapp2
from google.appengine.ext.zipserve import ZipHandler


class GBKitZipHandler(ZipHandler):
    """ HACK
        This method is overrided because we want to catch zip on zipserve folder
    """
    def get(self, prefix, name):
        import os

        self.name = name
        self.filename = name.split('/').pop()
        if os.path.exists('application/zipserve/' + prefix + '.zip'):
            self.ServeFromZipFile('application/zipserve/' + prefix + '.zip', name)
            return
        if os.path.exists('gbkit/zipserve/' + prefix + '.zip'):
            self.ServeFromZipFile('application/zipserve/' + prefix + '.zip', name)
            return
        if os.path.exists('zipserve/' + prefix + '.zip'):
            self.ServeFromZipFile('zipserve/' + prefix + '.zip', name)
            return
        if os.path.exists(prefix + '.zip'):
            self.ServeFromZipFile(prefix + '.zip', name)
            return
        super(GBKitZipHandler, self).get(prefix, name)

    """ HACK
        This method is overrided because we want to force the type guessing
    """
    def SetCachingHeaders(self):
        """ Mimetypes Init """
        from gbkit.mimetypes import mimetypes
        content_type, encoding = mimetypes.guess_type(self.filename, strict=False)
        if content_type:
            self.response.headers['Content-Type'] = str(content_type + \
                ('; charset=' + encoding if encoding else ''))
        super(GBKitZipHandler, self).SetCachingHeaders()

app = webapp2.WSGIApplication([('/([^/]+)/(.*)', GBKitZipHandler)])
