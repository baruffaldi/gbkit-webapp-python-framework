#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# GBKit
# - WebApps Python Handy Framework
#
# Copyright 2012 GB Consulting
# Author: Filippo Baruffaldi <filippo@baruffaldi.info>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
"""
@author: Filippo Baruffaldi <filippo@baruffaldi.info>
@organization: GB Consulting
@project: GBKit
@license: Apache 2.0
"""
__author__ = "Filippo Baruffaldi <filippo@baruffaldi.info>"
__organization__ = "GB Consulting"
__project__ = "GBKit"
__license__ = "Apache 2.0"
__url__ = "http://www.gbc-italy.com"
__version__ = 1

import os
import webapp
import google.appengine.api.users
import gbkit.models.user_settings

create_login_url = google.appengine.api.users.create_login_url
CreateLoginURL = create_login_url
create_logout_url = google.appengine.api.users.create_logout_url
CreateLogoutURL = create_logout_url
get_current_user = google.appengine.api.users.get_current_user
GetCurrentUser = get_current_user
is_current_user_admin = google.appengine.api.users.is_current_user_admin
IsCurrentUserAdmin = is_current_user_admin


# TODO estendere IterableUserDict.
class User(google.appengine.api.users.User):
    settings_data = None
    settings = {}

    device = {}
    useragent = os.environ.get("HTTP_USER_AGENT", None)
    accepted_charset = os.environ.get('HTTP_ACCEPT_CHARSET', None)
    accepted_language = os.environ.get('HTTP_ACCEPT_LANGUAGE', None)

    geo = {}
    geo_country = os.environ.get('HTTP_X_APPENGINE_COUNTRY', None)
    geo_region = os.environ.get('HTTP_X_APPENGINE_REGION', None)
    geo_city = os.environ.get('HTTP_X_APPENGINE_CITY', None)
    geo_latlong = os.environ.get('HTTP_X_APPENGINE_CITYLATLONG', None)

    ip = os.environ.get('REMOTE_ADDR', None)

    #auth_domain = os.environ.get('AUTH_DOMAIN', None)
    #id = os.environ.get('USER_ID', None)
    #email = os.environ.get('USER_EMAIL', None)
    #nickname = os.environ.get('USER_NICKNAME', None)
    #organization = os.environ.get('USER_ORGANIZATION', None)
    #user = os.environ.get('USER', None)

    def __init__(self, email='', _auth_domain=None, _user_id=None,
        federated_identity=None, federated_provider=None,
        _strict_mode=True):
        self.settings_data = gbkit.models.user_settings\
            .UserSettings.all().filter('user =', email).fetch(1000, 0)

        for setting in self.settings_data:
            self.settings[setting.name] = setting.value

        # Google defined part
        if _auth_domain is None:
            _auth_domain = os.environ.get('AUTH_DOMAIN')
        assert _auth_domain

        if email is None and federated_identity is None:
            email = os.environ.get('USER_EMAIL', email)
            _user_id = os.environ.get('USER_ID', _user_id)
            federated_identity = os.environ.get('FEDERATED_IDENTITY',
                                              federated_identity)
            federated_provider = os.environ.get('FEDERATED_PROVIDER',
                                              federated_provider)

        if email is None:
            email = ''

        if not email and not federated_identity and _strict_mode:
            raise google.appengine.api.users.UserNotFoundError

        self.__email = email
        self.__federated_identity = federated_identity
        self.__federated_provider = federated_provider
        self.__auth_domain = _auth_domain
        self.__user_id = _user_id or None

    @staticmethod
    def create_login_url(*args, **kwargs):
        return create_login_url(*args)

    @staticmethod
    def create_logout_url(*args, **kwargs):
        return create_logout_url(*args)

    @staticmethod
    def get_current_user():
        return get_current_user()

    @staticmethod
    def is_current_user_admin():
        return is_current_user_admin()

    def is_current_user_desktop(callback=None, *args, **kwargs):
        if 'Windows' in os.environ.get("HTTP_USER_AGENT"):
            if callback:
                callback(*args)
            return True
        return False


def login_required(handler_method):
    """A decorator to require that a user be logged in to access a handler.

    To use it, decorate your get() method like this:

    @login_required
    def get(self):
    user = users.get_current_user(self)
    self.response.out.write('Hello, ' + user.nickname())

    We will redirect to a login page if the user is not logged in. We always
    redirect to the request URI, and Google Accounts only redirects back as a GET
    request, so this should not be used for POSTs.
    """
    def check_login(self, *args, **kwargs):
        if self.request.method != 'GET':
            raise webapp.Error('The check_login decorator can only be used for GET requests')
        user = google.appengine.api.users.get_current_user()
        if not user:
            self.redirect(google.appengine.api.users.create_login_url(self.request.uri))
            return
        else:
            handler_method(self, *args)
    return check_login


def admin_required(handler_method):
    """A decorator to require that a user be logged in to access a handler.

    To use it, decorate your get() method like this:

    @login_required
    def get(self):
    user = users.get_current_user(self)
    self.response.out.write('Hello, ' + user.nickname())

    We will redirect to a login page if the user is not logged in. We always
    redirect to the request URI, and Google Accounts only redirects back as a GET
    request, so this should not be used for POSTs.
    """
    def check_login(self, *args, **kwargs):
        if self.request.method != 'GET':
            raise webapp.Error('The check_login decorator can only be used for GET requests')
        user = google.appengine.api.users.get_current_user()
        if not user.is_current_user_admin():
            self.redirect(google.appengine.api.users.create_login_url(self.request.uri))
            return
        else:
            handler_method(self, *args)
    return check_login
