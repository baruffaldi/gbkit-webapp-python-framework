#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# GBKit
# - WebApps Python Handy Framework
#
# Copyright 2012 GB Consulting
# Author: Filippo Baruffaldi <filippo@baruffaldi.info>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
"""
@author: Filippo Baruffaldi <filippo@baruffaldi.info>
@organization: GB Consulting
@project: GBKit
@license: Apache 2.0
"""
__author__ = "Filippo Baruffaldi <filippo@baruffaldi.info>"
__organization__ = "GB Consulting"
__project__ = "GBKit"
__license__ = "Apache 2.0"
__url__ = "http://www.gbc-italy.com"
__version__ = 1

import os


DEBUG = os.environ.get('SERVER_SOFTWARE').startswith("Dev")
if 'debug=1' in os.environ.get('QUERY_STRING', ''):
    DEBUG = True
if 'debug=0' in os.environ.get('QUERY_STRING', ''):
    DEBUG = False


def isiterable(obj):
    import collections
    if isinstance(obj, collections.Mapping):
        return True
    elif isinstance(obj, collections.Iterable):
        return True
    return False


def loads(string):
    return None  # TODO json.loads(pythonizer.encode(string, unpicklable, max_depth))


def dumps(obj, sort_keys=DEBUG, indent=DEBUG):
    from gbkit.formatter import json
    return """var results = %s;""" % json.dumps(obj,
            sort_keys, indent)


def load(stream):
    return None  # TODO pythonizer.decode(stream.read(), unpicklable, max_depth)


def dump(obj, stream, sort_keys=DEBUG, indent=DEBUG):
    from gbkit.formatter import json
    stream.write("""var results = %s;""" % json.dumps(obj,
            sort_keys, indent).read())
    return stream


def loads_s(string):
    return None  # TODO json.loads(pythonizer.encode(string, unpicklable, max_depth))


def dumps_s(obj, unpicklable=True, max_depth=None,
        sort_keys=DEBUG, indent=DEBUG):
    from gbkit.formatter import json
    return """var results = %s;""" % json.dumps_s(obj,
            sort_keys, indent)


def load_s(stream):
    return None  # TODO pythonizer.decode(stream.read(), unpicklable, max_depth)


def dump_s(obj, stream, unpicklable=True, max_depth=None,
        sort_keys=DEBUG, indent=DEBUG):
    from gbkit.formatter import json
    stream.write("""var results = %s;""" % json.dump_s(
        obj, unpicklable, max_depth, sort_keys, indent).read())
    return stream


encode = dumps
decode = loads
encode_s = dumps_s
decode_s = loads_s
