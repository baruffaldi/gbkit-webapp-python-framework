#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# GBKit
# - WebApps Python Handy Framework
#
# Copyright 2012 GB Consulting
# Author: Filippo Baruffaldi <filippo@baruffaldi.info>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
"""
@author: Filippo Baruffaldi <filippo@baruffaldi.info>
@organization: GB Consulting
@project: GBKit
@license: Apache 2.0
"""
__author__ = "Filippo Baruffaldi <filippo@baruffaldi.info>"
__organization__ = "GB Consulting"
__project__ = "GBKit"
__license__ = "Apache 2.0"
__url__ = "http://www.gbc-italy.com"
__version__ = 1

import os
import datetime
import time
import base64
from google.appengine.ext import db
from google.appengine.api import users, datastore_types
from google.appengine.ext.blobstore import blobstore
from gbkit.store.db import JsonProperty
DEBUG = os.environ.get('SERVER_SOFTWARE').startswith("Dev")
if 'debug=1' in os.environ.get('QUERY_STRING', ''):
    DEBUG = True
if 'debug=0' in os.environ.get('QUERY_STRING', ''):
    DEBUG = False


def isiterable(obj):
    import collections
    if isinstance(obj, collections.Mapping):
        return True
    elif isinstance(obj, collections.Iterable):
        return True
    return False

RAWTYPES = [
    None,
    bool(),
    int(), long(), float()
]

RAWINSTANCES = [
    unicode, str,
    datastore_types.Text,
    datastore_types.Category,
    datastore_types.Link
]

PYTHON_DATETIME_FORMATS = [
    '%a', '%A', '%b', '%B', '%c', '%d',
    '%f', '%H', '%I', '%j', '%m', '%M',
    '%p', '%S', '%U', '%w', '%W', '%x',
    '%X', '%y', '%Y', '%z', '%Z'
]

class Encoder(object):
    def gqltype2pythontype(objj, default=None, skip_properties=[], b64=False, encoding='utf-8', feed_no_blobs=False):
        import logging
        def encoder(input):
            if isinstance(input, db.GqlQuery):
                return list(input)

            elif isinstance(input, db.Model):
                obj = input
                if feed_no_blobs:
                    skip_properties = skip_properties if skip_properties else getattr(obj, '_skip_properties', [])
                else:
                    skip_properties = skip_properties if skip_properties else getattr(obj, '_json_skip_properties', [])

                d_value = dict()
                d_value.setdefault('key', str(obj.key()))

                for token in obj.properties():
                    if not token.startswith('_') and token not in skip_properties:
                        value = encoder(getattr(obj, token), default, skip_properties, b64, encoding, feed_no_blobs)
                        if (token in obj.referencies() or token in obj.user_referencies()) and getattr(obj, token) in [default, None] or (getattr(obj, token) is None and token in obj._json_object_if_none_properties):
                            value = {}
                        d_value.setdefault(token, value)

                for token in obj._callable_properties:
                    if not token.startswith('_') and token not in skip_properties:
                        value = encoder(getattr(obj, token)(), default, skip_properties, b64, encoding, feed_no_blobs)
                        d_value.setdefault(token, value)

                for model in obj._collections:
                    if not model.startswith('_') and model not in skip_properties:
                        if hasattr(obj, model) and getattr(obj, model).count(1):
                            #if not getattr(obj, '_'+model+'_entity', False):
                            #    setattr(obj, '_'+model+'_entity', getattr(obj, model).order('-date').fetch(1)[0])

                            #content = getattr(obj, '_'+model+'_entity')
                            contents = getattr(obj, model).order('-date').fetch(1000)
                            value = []
                            for content in contents:
                                t_value = encoder(content, default, content._json_skip_properties if feed_no_blobs else content._skip_properties, b64, encoding, feed_no_blobs)

                                for token in content._callable_properties:
                                    if not token.startswith('_') and token not in content._skip_properties:
                                        v = encoder(getattr(content, token)(), default, content._json_skip_properties if feed_no_blobs else content._skip_properties, b64, encoding, feed_no_blobs)
                                        t_value.setdefault(token, v)
                                value.append(t_value)
                        else:
                            value = []
                        d_value.setdefault(model, value)

                for model in obj._collections_single_reference:
                    if not model.startswith('_') and model not in skip_properties:
                        if hasattr(obj, model) and getattr(obj, model).count(1):
        #                        value = memcache.get(str(obj.key())+'_'+model+'_pythonized')
        #                        if value is None:
        #                            content = getattr(obj, model).order('-date').fetch(1)[0]
        #                            value = encoder(content, default, content._json_skip_properties if feed_no_blobs else content._skip_properties, b64, encoding, feed_no_blobs)
        #                            for token in content._callable_properties:
        #                                if not token.startswith('_') and token not in content._skip_properties:
        #                                    v = encoder(getattr(content, token)(), default, content._json_skip_properties if feed_no_blobs else content._skip_properties, b64, encoding, feed_no_blobs)
        #                                    value.setdefault(token, v)
        #                            memcache.set(str(obj.key())+'_'+model+'_pythonized', value, 60*3)

                            content = getattr(obj, model).order('-date').fetch(1)[0]
                            value = encoder(content, default, content._json_skip_properties if feed_no_blobs else content._skip_properties, b64, encoding, feed_no_blobs)

                            for token in content._callable_properties:
                                if not token.startswith('_') and token not in content._skip_properties:
                                    v = encoder(getattr(content, token)(), default, content._json_skip_properties if feed_no_blobs else content._skip_properties, b64, encoding, feed_no_blobs)
                                    value.setdefault(token, v)

                        else:
                            value = {}
                        d_value.setdefault(model, value)

                return d_value

            elif isinstance(input, datetime.datetime):
                output = {}
                fields = ['day', 'hour', 'microsecond', 'minute', 'month', 'second',
                'year']
                methods = ['ctime', 'isocalendar', 'isoformat', 'isoweekday',
                'timetuple']
                for field in fields:
                    output[field] = getattr(input, field)
                for method in methods:
                    output[method] = getattr(input, method)()
                output['epoch'] = time.mktime(input.timetuple())
                return output

            elif isinstance(input, time.struct_time):
                return list()

            elif isinstance(input, users.User):
                output = {}
                methods = ['nickname', 'email', 'auth_domain']
                for method in methods:
                    output[method] = getattr(input, method)()
                return output


            elif str(input.__class__) == "webob.multidict.UnicodeMultiDict":
                return list()

            elif isinstance(input, datastore_types.Key):
                return str(input)

            elif isinstance(input, blobstore.BlobInfo):
                return str(input.key())

            elif isinstance(input, datastore_types.Blob):
                if b64:
                    return base64.b64encode(input)
                return input  # .encode(encoding)

            elif isinstance(input, JsonProperty):
                return input

            elif isinstance(input, dict):
                output = {}
                for key, value in input.iteritems():
                    output.setdefault(key, gqltype2pythontype(value, default, skip_properties, b64, encoding, feed_no_blobs))
                return output

            elif isinstance(input, list):
                ret = list()
                for x in input:
                    ret.append(gqltype2pythontype(x, default, skip_properties, b64, encoding, feed_no_blobs))
                return ret

            elif type(input) in [type(x) for x in RAWTYPES]:
                return input

            elif True in [isinstance(input, x) for x in RAWINSTANCES]:
                return input.encode(encoding)


            try:
                logging.warn("[GBKit] Pythonizer fails with: %s (%s)"%(type(input), str(input)))
            except:
                logging.warn("[GBKit] Pythonizer fails with: %s"%type(input))
            return default
        encoder(objj)

def encode(obj, unpicklable=True, max_depth=None):
    res = Encoder().gqltype2pythontype(obj)
    return res
