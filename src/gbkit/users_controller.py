#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# GBKit
# - WebApps Python Handy Framework
#
# Copyright 2012 GB Consulting
# Author: Filippo Baruffaldi <filippo@baruffaldi.info>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
"""
@author: Filippo Baruffaldi <filippo@baruffaldi.info>
@organization: GB Consulting
@project: GBKit
@license: Apache 2.0
"""
__author__ = "Filippo Baruffaldi <filippo@baruffaldi.info>"
__organization__ = "GB Consulting"
__project__ = "GBKit"
__license__ = "Apache 2.0"
__url__ = "http://www.gbc-italy.com"
__version__ = 1

from gbkit.wsgi import WSGIApplication
from gbkit.wsgi.handlers import RequestHandler, DEBUG, wsgi_config

from google.appengine.api.users import create_login_url
from google.appengine.api.users import create_logout_url


class LoginLogout(RequestHandler):
    def get(self, method='login', domain=None, *args):
        providers = self._gbkit_settings.OPENID_PROVIDERS
        requested_url = self.request.get('url', default_value= \
            self.environment.headers.get('HTTP_REFERER', '/'))
        domain = domain if domain[1:] else None

        if method.endswith('logout'):
            self.redirect(create_logout_url(requested_url, domain))
        else:
            urls = []
            for p_name in providers:
                p_url = providers[p_name][0].lower()
                url = (create_login_url(requested_url,
                        federated_identity=p_url),
                    p_name)
                urls.append(url)
            urls.reverse()
            self.render(self.get_template('login.html'), {'urls': urls})


class NotFound(RequestHandler):
    def get(self):
        self.raise_error(code=404)


routes = [
    (r'.*(conflogout.*|logout.*|conflogin.*|login.*)(|/.*)', LoginLogout),
    (r'.*', NotFound)
]
app = WSGIApplication(routes, debug=DEBUG, config=wsgi_config)
