#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# GBKit
# - WebApps Python Handy Framework
#
# Copyright 2012 GB Consulting
# Author: Filippo Baruffaldi <filippo@baruffaldi.info>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
"""
@author: Filippo Baruffaldi <filippo@baruffaldi.info>
@organization: GB Consulting
@project: GBKit
@license: Apache 2.0
"""
__author__ = "Filippo Baruffaldi <filippo@baruffaldi.info>"
__organization__ = "GB Consulting"
__project__ = "GBKit"
__license__ = "Apache 2.0"
__url__ = "http://www.gbc-italy.com"
__version__ = 1

import datetime
from tornadocookies import LilCookies
import email.utils
import calendar
import re
import Cookie


class Factory(LilCookies):
    def __setitem__(self, name, value):
        return self.set_cookie(name,
            value if isinstance(value, str) else str(value))

    def __getitem__(self, name):
        return self.get_cookie(name)

    def set(self, name, value, *args, **kwargs):
        return self.set_cookie(name,
            value if isinstance(value, str) else str(value), **kwargs)

    def get(self, name, *args, **kwargs):
        return self.get_cookie(name, *args, **kwargs)

    def set_cookie(self, name, value=None, domain=None, expires=None,
            path="/", expires_days=None, secure=False, **kwargs):
        if value is None:
            value = 'deleted'
            expires = datetime.timedelta(minutes=-50000)

        cookie = self.cookies()
        if cookie:
            if expires:
                if isinstance(expires, datetime.timedelta):
                    expires = datetime.datetime.now() + expires
                if isinstance(expires, datetime.datetime):
                    expires = expires.strftime('%a, %d %b %Y %H:%M:%S')
                expires = (expires - datetime.datetime.now()).days

            if secure:
                return self.set_secure_cookie(name, value, \
                    domain, expires, path, \
                    expires_days, **kwargs)

            # GBKit
            # Part taken from tornadocookies.py
            """Sets the given cookie name/value with the given options.

            Additional keyword arguments are set on the Cookie.Morsel
            directly.
            See http://docs.python.org/library/cookie.html#morsel-objects
            for available attributes.
            """
            name = LilCookies._utf8(name)
            value = LilCookies._utf8(value)
            if re.search(r"[\x00-\x20]", name + value):
                # Don't let us accidentally inject bad stuff
                raise ValueError("Invalid cookie %r: %r" % (name, value))
            if not hasattr(self, "_new_cookies"):
                self._new_cookies = []
            new_cookie = Cookie.BaseCookie()
            self._new_cookies.append(new_cookie)
            new_cookie[name] = value
            if domain:
                new_cookie[name]["domain"] = domain
            if expires_days is not None and not expires:
                expires = datetime.datetime.utcnow() + datetime.timedelta(days=expires_days)
            if expires:
                timestamp = calendar.timegm(expires.utctimetuple())
                new_cookie[name]["expires"] = email.utils.formatdate(
                timestamp, localtime=False, usegmt=True)
            if path:
                new_cookie[name]["path"] = path
            for k, v in kwargs.iteritems():
                new_cookie[name][k] = v

            # The 2 lines below were not in Tornado.  Instead, they output all their cookies to the headers at once before a response flush.
            for vals in new_cookie.values():
                self.response.headers.add('Set-Cookie', vals.OutputString(None))
