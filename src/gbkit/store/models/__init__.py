#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# GBKit
# - WebApps Python Handy Framework
#
# Copyright 2012 GB Consulting
# Author: Filippo Baruffaldi <filippo@baruffaldi.info>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
"""
@author: Filippo Baruffaldi <filippo@baruffaldi.info>
@organization: GB Consulting
@project: GBKit
@license: Apache 2.0
"""
__author__ = "Filippo Baruffaldi <filippo@baruffaldi.info>"
__organization__ = "GB Consulting"
__project__ = "GBKit"
__license__ = "Apache 2.0"
__url__ = "http://www.gbc-italy.com"
__version__ = 1

import datetime
import time
import base64
from google.appengine.ext import db as gaedb
from google.appengine.api import users, datastore_types
from google.appengine.ext.blobstore import blobstore
from gbkit.store.db import JsonProperty
from gbkit.formatter.json import encode_s as json_encode
from gbkit.formatter.yaml import encode_s as yaml_encode


class Model(gaedb.Expando):
    _properties_mapping = {
        "<class 'google.appengine.ext.db.FloatProperty'>": float,
        "<class 'google.appengine.ext.db.IntegerProperty'>": int,
        "<class 'google.appengine.ext.db.BooleanProperty'>": bool
    }
    _protected = ['key']
    _collections = []
    _collections_single_reference = []
    _base64_properties = []
    _skip_properties = []
    _json_skip_properties = []
    _json_object_if_none_properties = []  # Todo
    _callable_properties = []

    """ Default Properties """
    user = gaedb.UserProperty(auto_current_user_add=True)
    date = gaedb.DateTimeProperty(auto_now_add=True)
    update_date = gaedb.DateTimeProperty(auto_now_add=True, auto_now=True)
    update_user = gaedb.UserProperty(auto_current_user=True, auto_current_user_add=True)

    disabled = gaedb.BooleanProperty(default=False)

    def __init__(self, *args, **kwargs):
        kwargs = self.intelligent_update(kwargs)
        super(Model, self).__init__(*args, **kwargs)

    def intelligent_update(self, new_values):
        ret = {}
        for key in new_values:
            ret[key] = self.intelligent_set(key, new_values[key])
        return ret

    def intelligent_set(self, key, value):
        if value is None:
            return None
        if key in self.properties().keys():
            proptype = str(type(self.properties()[key]))
            if proptype in self._properties_mapping.keys():
                return self._properties_mapping[proptype](value)
        return value

    def get_attr(self, key, safe=True):
        if self.is_empty(key, safe):
            return None

        if type(self.__getattr__(key)) == type(str()) and safe:
            return self.__getattr__(key).strip()
        return self.__getattr__(key)

    # TODO: Uso di hasattr() ???
    def is_empty(self, key, safe=True):
        if not key in self.__dict__():
            return False

        return self.is_empty_string(key, safe) or self.is_none(key)

    def is_empty_string(self, key, safe=True):
        if not key in self.__dict__():
            return False

        if type(self.__getattr__(key)) == type(str()) and safe:
            return self.__getattr__(key).strip() == ''
        return self.__getattr__(key) == ''

    def is_none(self, key):
        if not key in self.__dict__():
            return False

        return self.__getattr__(key) is None

    @property
    def permalink(self):
        return self.key()

    def prev(self):
        if self.all().filter('__key__ <', self.key()).count(1):
            return self.all().filter('__key__ <', self.key()).order('-__key__').fetch(1)[0]
        return {}

    def next(self):
        if self.all().filter('__key__ >', self.key()).count(1):
            return self.all().filter('__key__ >', self.key()).order('__key__').fetch(1)[0]
        return {}

    _referencies = []

    def referencies(self):
        if not self._referencies:
            for x, y in self.properties().items():
                if isinstance(y, gaedb.ReferenceProperty):
                    self._referencies.append(x)
        return self._referencies

    _user_referencies = []

    def user_referencies(self):
        if not self._user_referencies:
            for x, y in self.properties().items():
                if isinstance(y, gaedb.UserProperty):
                    self._user_referencies.append(x)
        return self._user_referencies

    def __pythonize__(self, collections=False, default=None, skip_properties=[], b64=False, encoding='utf-8', feed_no_blobs=False):
        if feed_no_blobs:
            skip_properties = skip_properties if skip_properties else self._json_skip_properties
        else:
            skip_properties = skip_properties if skip_properties else self._skip_properties

        d_value = dict()
        d_value.setdefault('key', str(self.key()))

        for token in self.properties():
            if not token.startswith('_') and token not in skip_properties:
                value = json_encode(getattr(self, token), default, skip_properties, b64, encoding, feed_no_blobs)
                if (token in self.referencies() or token in self.user_referencies()) and getattr(self, token) in [default, None] or (getattr(self, token) is None and token in self._json_object_if_none_properties):
                    value = {}
                d_value.setdefault(token, value)

        for token in self._callable_properties:
            if not token.startswith('_') and token not in skip_properties:
                value = json_encode(getattr(self, token)(), default, skip_properties, b64, encoding, feed_no_blobs)
                d_value.setdefault(token, value)

        if collections:
            for model in self._collections:
                if not model.startswith('_') and model not in skip_properties:
                    if hasattr(self, model) and getattr(self, model).count(1):
                        #if not getattr(self, '_'+model+'_entity', False):
                        #    setattr(self, '_'+model+'_entity', getattr(self, model).order('-date').fetch(1)[0])

                        #content = getattr(self, '_'+model+'_entity')
                        contents = getattr(self, model).order('-date').fetch(1000)
                        value = []
                        for content in contents:
                            t_value = json_encode(content, default, content._json_skip_properties if feed_no_blobs else content._skip_properties, b64, encoding, feed_no_blobs)

                            for token in content._callable_properties:
                                if not token.startswith('_') and token not in content._skip_properties:
                                    v = json_encode(getattr(content, token)(), default, content._json_skip_properties if feed_no_blobs else content._skip_properties, b64, encoding, feed_no_blobs)
                                    t_value.setdefault(token, v)
                            value.append(t_value)
                    else:
                        value = []
                    d_value.setdefault(model, value)

            for model in self._collections_single_reference:
                if not model.startswith('_') and model not in skip_properties:
                    if hasattr(self, model) and getattr(self, model).count(1):
#                        value = memcache.get(str(self.key())+'_'+model+'_pythonized')
#                        if value is None:
#                            content = getattr(self, model).order('-date').fetch(1)[0]
#                            value = json_encode(content, default, content._json_skip_properties if feed_no_blobs else content._skip_properties, b64, encoding, feed_no_blobs)
#                            for token in content._callable_properties:
#                                if not token.startswith('_') and token not in content._skip_properties:
#                                    v = json_encode(getattr(content, token)(), default, content._json_skip_properties if feed_no_blobs else content._skip_properties, b64, encoding, feed_no_blobs)
#                                    value.setdefault(token, v)
#                            memcache.set(str(self.key())+'_'+model+'_pythonized', value, 60*3)

                        content = getattr(self, model).order('-date').fetch(1)[0]
                        value = json_encode(content, default, content._json_skip_properties if feed_no_blobs else content._skip_properties, b64, encoding, feed_no_blobs)

                        for token in content._callable_properties:
                            if not token.startswith('_') and token not in content._skip_properties:
                                v = json_encode(getattr(content, token)(), default, content._json_skip_properties if feed_no_blobs else content._skip_properties, b64, encoding, feed_no_blobs)
                                value.setdefault(token, v)

                    else:
                        value = {}
                    d_value.setdefault(model, value)

        return d_value

    def __dict__(self, *args, **kwargs):
        return json_encode(**args)

    def to_dict(self, *args, **kwargs):
        return json_encode(**args)

    def pythonize(self, *args, **kwargs):
        return json_encode(**args)

    def get_data(self, collections=True, default=None, skip_properties=None, b64=False, encoding='utf-8'):
        skip_properties = skip_properties if skip_properties is not None else self._skip_properties
        return json_encode(collections, default, skip_properties, b64, encoding, False)

    def fetch(self, limit=1000, offset=0, **kwargs):
        if limit <= 0:
            limit = None
        if offset <= -1:
            offset = 0
        return super(Model, self).fetch(limit, offset, **kwargs)


class SerializableModel(Model):
    def get_data(self, collections=True, default=None, skip_properties=None, b64=False, encoding='utf-8'):
        return super(SerializableModel, self).get_data(collections, default, skip_properties, b64, encoding)

    def to_json(self, *args, **kwargs):
        return json_encode(self.__json__(*args, **kwargs))

    def to_yaml(self, *args, **kwargs):
        return yaml_encode(json_encode(collections, default, skip_properties, b64, encoding, True))

    # _skip_blobs = True # parameter???
    """
    @TODO :
    - yaml
    - pdf
    - rss
    - atom
    - xml
    - raw
    - html

    # TODO:  rss does not include
    yaml_does_not_include = []
    def to_yaml(self, attr_list=[]):
        return None

    # TODO:  json does not include
    json_does_not_include = []
    def to_json(self, attr_list=[]):
        def to_entity(entity):
            self._to_entity(self.__dict__)
            replace_datastore_types(self.__dict__)
        values = to_dict(self, attr_list, to_entity)
        return simplejson.dumps(values)

    # TODO:  rss does not include
    rss_does_not_include = []
    def to_rss(self, attr_list=[]):
        return None

    # TODO:  rss does not include
    atom_does_not_include = []
    def to_atom(self, attr_list=[]):
        return None

    # TODO:  rss does not include
    xml_does_not_include = []
    def to_xml(self, attr_list=[]):
        return None

    # TODO: per liste: Indirizzi /feeds/xml/blah
    # TODO: per singoli: Indirizzi getter di default + get variable FORMAT=xml

    def to_model(self, attr_list=[]):
        def to_entity(entity):
            self._to_entity(self.__dict__)
            replace_datastore_types(self.__dict__)
        values = to_dict(self, attr_list, to_entity)
        return simplejson.dumps(values)

    _feed_mapping = {}
    _feed = {}
    def to_feed(self, attr_list=[]):
        feed_fields = [
            'title','description'
        ]
        for field in feed_fields:
            if self.__getattribute__(self._feed_mapping.get(field)):
                self._feed.setdefault(field, self.__getattribute__(self._feed_mapping.get(field)))

        return self._feed

    _common_entity_mapping = {}
    _common_entity = {}
    def to_common_entity(self, attr_list=[]):
        common_entity_fields = [
            'title','description'
        ]
        for field in common_entity_fields:
            if self.__getattribute__(self._common_entity_mapping.get(field)):
                self._common_entity.setdefault(field, self.__getattribute__(self._common_entity_mapping.get(field)))

        return self._common_entity"""



class RecursiveSerializableModel(SerializableModel):
    def get_data(self, collections=True, default=None, skip_properties=None, b64=False, encoding='utf-8'):
        return super(SerializableModel, self).get_data(collections, default, skip_properties, b64, encoding)