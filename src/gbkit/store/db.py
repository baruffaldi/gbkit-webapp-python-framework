#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# GBKit
# - WebApps Python Handy Framework
#
# Copyright 2012 GB Consulting
# Author: Filippo Baruffaldi <filippo@baruffaldi.info>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
"""
@author: Filippo Baruffaldi <filippo@baruffaldi.info>
@organization: GB Consulting
@project: GBKit
@license: Apache 2.0
"""
__author__ = "Filippo Baruffaldi <filippo@baruffaldi.info>"
__organization__ = "GB Consulting"
__project__ = "GBKit"
__license__ = "Apache 2.0"
__url__ = "http://www.gbc-italy.com"
__version__ = 1

""" Reserved methods:
    all,app,copy,delete,entity,entity_type,fields
    from_entity,get,gql,instance_properties,is_saved
    key,key_name,kind,parent,parent_key,properties
    put,setdefault,to_xml,update
"""

from google.appengine.ext import db as gaedb

from gbkit.formatter.json import encode_s as json_encode, decode_s as json_decode

from gbkit.contrib.aetycoon import DerivedProperty, LowerCaseProperty, LengthProperty, TransformProperty, KeyProperty, PickleProperty, SetProperty, CurrentDomainProperty, ChoiceProperty, CompressedProperty, CompressedBlobProperty, CompressedTextProperty, ArrayProperty

GBKitDerivedProperty = DerivedProperty
GBKitLowerCaseProperty = LowerCaseProperty
GBKitLengthProperty = LengthProperty
GBKitTransformProperty = TransformProperty
GBKitKeyProperty = KeyProperty
GBKitPickleProperty = PickleProperty
GBKitSetProperty = SetProperty
GBKitCurrentDomainProperty = CurrentDomainProperty
GBKitChoiceProperty = ChoiceProperty
GBKitCompressedProperty = CompressedProperty
GBKitCompressedBlobProperty = CompressedBlobProperty
GBKitCompressedTextProperty = CompressedTextProperty
GBKitArrayProperty = ArrayProperty


class GBKitUpperCaseTextProperty(gaedb.TextProperty):
    def get_value_for_datastore(self, model_instance):
        result = super(GBKitUpperCaseTextProperty, self).get_value_for_datastore(model_instance) or ''
        return gaedb.Text(result.upper())


class GBKitUpperCaseStringProperty(gaedb.StringProperty):
    def validate(self, value):
        return value

    def get_value_for_datastore(self, model_instance):
        result = super(GBKitUpperCaseStringProperty, self).get_value_for_datastore(model_instance)
        if not result:
            return None
        return result.upper()


class JsonProperty(gaedb.TextProperty):
    def validate(self, value):
        return value

    def get_value_for_datastore(self, model_instance):
        result = super(JsonProperty, self).get_value_for_datastore(model_instance)
        result = json_encode(result)
        return gaedb.Text(result)

    def make_value_from_datastore(self, value):
        try:
            value = json_decode(str(value))
        except:
            pass

        return super(JsonProperty, self).make_value_from_datastore(value)
GBKitJsonProperty = JsonProperty

class GBKitListProperty(gaedb.ListProperty):
    def validate(self, value):
        """Validate list.

        Returns:
        A valid value.

        Raises:
        BadValueError if property is not a list whose items are instances of
        the item_type given to the constructor.
        """

        if value is not None:
            if not isinstance(value, list):
                value = [value]
        return super(GBKitListProperty, self).validate(value)


class GBKitStringListProperty(gaedb.StringListProperty):
    def validate(self, value):
        """Validate list.

        Returns:
        A valid value.

        Raises:
        BadValueError if property is not a list whose items are instances of
        the item_type given to the constructor.
        """

        if value is not None:
            if not isinstance(value, list):
                value = [value]
        return super(GBKitStringListProperty, self).validate(value)
        #value = self.validate_list_contents(value)
        #return value
#    def get_value_for_datastore(self, model_instance):
#        result = super(JsonProperty, self).get_value_for_datastore(model_instance)
#        result = json_encode(result)
#        return gaedb.Text(result)

#    def make_value_from_datastore(self, value):
#        try:
#            value = json_decode(str(value))
#        except:
#            pass
#
#        return super(JsonProperty, self).make_value_from_datastore(value)
