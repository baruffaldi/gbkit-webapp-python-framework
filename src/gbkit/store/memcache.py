#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# GBKit
# - WebApps Python Handy Framework
#
# Copyright 2012 GB Consulting
# Author: Filippo Baruffaldi <filippo@baruffaldi.info>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
"""
@author: Filippo Baruffaldi <filippo@baruffaldi.info>
@organization: GB Consulting
@project: GBKit
@license: Apache 2.0
"""
__author__ = "Filippo Baruffaldi <filippo@baruffaldi.info>"
__organization__ = "GB Consulting"
__project__ = "GBKit"
__license__ = "Apache 2.0"
__url__ = "http://www.gbc-italy.com"
__version__ = 1

try:
    from google.appengine.api import memcache
except:
    import memcache

from gbkit.datatypes import Object


class Factory(Object):
    def __setitem__(self, key, value=None):
        if self.has(key):
            memcache.set(key, value)
        else:
            memcache.add(key, value)

    def __getitem__(self, *args, **kwargs):
        return memcache.get(*args, **kwargs)

    def set(self, key, value, **kwargs):
        if self.has(key):
            memcache.set(key, value, **kwargs)
        else:
            memcache.add(key, value, **kwargs)

    def add(self, *args, **kwargs):
        return memcache.add(*args, **kwargs)

    def delete(self, *args, **kwargs):
        return memcache.delete(*args, **kwargs)

    def has(self, *args, **kwargs):
        return memcache.get(*args, **kwargs) is not None

    def get(self, *args, **kwargs):
        return memcache.get(*args, **kwargs)

    def update(self, *args, **kwargs):
        return memcache.set(*args, **kwargs)
