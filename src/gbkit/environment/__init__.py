#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# GBKit
# - WebApps Python Handy Framework
#
# Copyright 2012 GB Consulting
# Author: Filippo Baruffaldi <filippo@baruffaldi.info>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
"""
@author: Filippo Baruffaldi <filippo@baruffaldi.info>
@organization: GB Consulting
@project: GBKit
@license: Apache 2.0
"""
__author__ = "Filippo Baruffaldi <filippo@baruffaldi.info>"
__organization__ = "GB Consulting"
__project__ = "GBKit"
__license__ = "Apache 2.0"
__url__ = "http://www.gbc-italy.com"
__version__ = 1

import os
import gbkit_config
from gbkit.datatypes import Object


class Factory(Object):
    def __init__(self, *args, **kwargs):
        self.headers = {}
        for key in os.environ.keys():
            self.headers[key.replace('.', '_')] = os.environ.get(key)
        self.google_apps = 'HTTP_X_GOOGLE_APPS_METADATA' in os.environ
        if self.google_apps:
            self.google_apps_domain = os.environ.get(
                'HTTP_X_GOOGLE_APPS_METADATA').split('=')[1]

        self.cookies = os.environ.get('HTTP_COOKIE', False)
        self.django_settings_module = os.environ.get('DJANGO_SETTINGS_MODULE', False)
        self.thread_safe = os.environ.get('_AH_THREADSAFE', False)
        self.wsgi_version = os.environ.get('wsgi.version', False)
        self.wsgi_run_once = os.environ.get('wsgi.run_once', False)
        self.wsgi_errors = os.environ.get('wsgi.errors', False)
        self.wsgi_url_scheme = os.environ.get('wsgi.url_scheme', False)
        self.multiprocess = os.environ.get('wsgi.multiprocess', False)
        self.multithread = os.environ.get('wsgi.multithread', False)
        self.cache_control = os.environ.get('HTTP_CACHE_CONTROL', False)
        self.gateway_interface = os.environ.get('GATEWAY_INTERFACE', False)
        self.referer = os.environ.get('HTTP_REFERER', False)
        self.protocol = os.environ.get('SERVER_PROTOCOL', 'HTTP/1.1')
        self.method = os.environ.get('REQUEST_METHOD', 'GET')
        self.tz = os.environ.get('TZ', 'UTC')
        self.name = os.environ.get('SERVER_NAME', False)
        self.id = os.environ.get('INSTANCE_ID', False)
        self.id_hash = os.environ.get('REQUEST_ID_HASH', False)
        self.log_id = os.environ.get('REQUEST_LOG_ID', False)
        self.host = os.environ.get('HTTP_HOST', False)
        self.domain = ".".join(os.environ.get('HTTP_HOST', False).split('.')[:2])
        self.port = os.environ.get('SERVER_PORT', False)
        self.is_ssl = os.environ.get('HTTPS', False) != 'off'
        self.is_gae = 'Google App Engine' in os.environ.get('SERVER_SOFTWARE', '')
        if self.is_gae:
            self.gae_app_id = os.environ.get('APPLICATION_ID', False).split('~')[-1]
            self.gae_datacenter = os.environ.get('DATACENTER', False)
            self.gae_runtime = os.environ.get('APPENGINE_RUNTIME', False)
            self.gae_version = os.environ.get('CURRENT_VERSION_ID', False)
            self.gae_milestone = os.environ.get('CURRENT_VERSION_ID', False).split('.')[0]
            self.gae_datastore_active = os.environ.get('__DATASTORE_CONNECTION_INITIALIZED__', False) == '1'
            self.gae_default_namespace = os.environ.get('HTTP_X_APPENGINE_DEFAULT_NAMESPACE')
            self.gae_default_version_hostname = os.environ.get('DEFAULT_VERSION_HOSTNAME')

    @property
    def basepath(self):
        return gbkit_config.BASEPATH

    @property
    def versions(self):
        return gbkit_config.versions()

    @property
    def env(self):
        return 'public' if 'admin' not in os.environ.get('PATH_INFO', '/').split('/')[-1:] != 'admin' else 'admin'

    @property
    def environ(self):
        return os.environ

    @property
    def safe_environ(self):
        newlist = {}
        for key in os.environ.keys():
            newlist.setdefault(key.lower().replace('-', '_'), os.environ.get(key, ''))
        return newlist

    @property
    def environment(self):
        newlist = []
        for key in os.environ.keys():
            newlist.append({'name': key, 'value': os.environ.get(key, '')})
        return newlist

    @property
    def is_public(self):
        return self.env == 'public'

    @property
    def is_admin(self):
        return self.env == 'admin'

    @property
    def is_production(self):
        return not os.environ.get('SERVER_SOFTWARE', '').startswith("Dev")

    @property
    def is_development(self):
        return os.environ.get('SERVER_SOFTWARE', '').startswith("Dev")
