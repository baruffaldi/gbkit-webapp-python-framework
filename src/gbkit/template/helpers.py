#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# GBKit
# - WebApps Python Handy Framework
#
# Copyright 2012 GB Consulting
# Author: Filippo Baruffaldi <filippo@baruffaldi.info>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
"""
@author: Filippo Baruffaldi <filippo@baruffaldi.info>
@organization: GB Consulting
@project: GBKit
@license: Apache 2.0
"""
__author__ = "Filippo Baruffaldi <filippo@baruffaldi.info>"
__organization__ = "GB Consulting"
__project__ = "GBKit"
__license__ = "Apache 2.0"
__url__ = "http://www.gbc-italy.com"
__version__ = 1

import re
import datetime
import time

from google.appengine.ext.webapp import template
from google.appengine.ext import db
from google.appengine.api import users, datastore_types

from django.template import TemplateSyntaxError, Node
from django.template.defaultfilters import stringfilter

import yaml as yaml_encoder
#from gbkit.formatter.json import encode as json_encode
#from gbkit.formatter.json import decode_s
from gbkit.template import EntityNode, EntitiesNode

register = template.create_template_register()

#<script type="text/javascript" src="https://www.google.com/recaptcha/api/challenge?k=6LeY5cwSAAAAAGb0zrzGz65gnrhRgdDRW-W6-VwZ"></script>
#<noscript><iframe src="https://www.google.com/recaptcha/api/noscript?k=6LeY5cwSAAAAAGb0zrzGz65gnrhRgdDRW-W6-VwZ" height="300" width="500" frameborder="0"></iframe><br></noscript>

@register.filter
def truncatechars(value, token):
    if len(value) <= int(token):
        return value
    return value[:int(token)] + '...'

@register.filter
def to_dict(value):
    return dict(value)

@register.filter
def extract_domain_from_url(value):
    return ' . '.join(value.split('/')[2].split('.')[-3:]).split(':')[0] if value else ''

@register.filter
def json(value):
    return json_encode(value, strict=False)

@register.filter
def json_decode(value):
    return decode_s(value, ensure_ascii=False)

@register.filter
def yaml(value):
    from yaml import encode as yaml_encoder
    return yaml_encoder(value)

@register.filter
def xml(obj):
    if isinstance(obj, db.GqlQuery):
        output = "</value><value>".join(list(obj))
        return "<list>"+output[7:]+"</list>"

    elif type(obj) in [type(int),type(float),type(bool),type(tuple)]:
        return str(obj)

    elif isinstance(obj, db.Model):
        properties = obj.properties().items()
        output = "<entity><key>"+str(obj.key())+"</key>"
        for field, value in properties:
            output += xml(getattr(obj, field, None))
        return output+"</entity>"

    elif isinstance(obj, dict):
        properties = obj.keys()
        output = "<dict>"
        for field in properties:
            output += "<"+field+">"+xml(getattr(obj, field, obj[field]))+"</"+field+">"
        return output+"</dict>"

    elif isinstance(obj, datetime.datetime):
        output = "<datetime>"
        fields = ['day', 'hour', 'microsecond', 'minute', 'month', 'second',
                'year']
        methods = ['ctime', 'isocalendar', 'isoformat', 'isoweekday',
                'timetuple']
        for field in fields:
            output += "<"+field+">"+str(getattr(obj, field))+"</"+field+">"
        for method in methods:
            output += "<"+method+">"+str(getattr(obj, method)())+"</"+method+">"
        return output+"<epoch>"+str(time.mktime(obj.timetuple()))+"</epoch></datetime>"

    elif isinstance(obj, time.struct_time):
        output = "</value><value>".join(list(obj))
        return "<list>"+output[7:]+"</list>"

    elif isinstance(obj, users.User):
        output = "<user>"
        methods = ['nickname', 'email', 'auth_domain']
        for method in methods:
            output += "<"+method+">"+str(getattr(obj, method)())+"</"+method+">"
        return output+"</user>"

    elif isinstance(obj, datastore_types.Key):
        return "<key>"+str(obj)+"</key>"
    return ""

@register.filter
def get_parameters(value):
    if getattr(value, 'parameters', False):
        return value.parameters()

    if getattr(value, 'properties', False):
        return value.properties()

    if getattr(value, 'keys', False):
        return value.keys()

    return []

@register.filter
def get_parameter(value, token):
    if getattr(value, token, False):
        if getattr(getattr(value, token), '__call__', False):
            return getattr(value, token)()
        else:
            return getattr(value, token)

    return None

@register.filter
def get_type(value):
    if value:
        if len(str(type(value)).split("'")) >= 2:
#            if str(type(value)).split("'")[1] == 'datetime.datetime':
#                return 'date'
            if str(type(value)).split("'")[1] == 'unicode':
                return 'string'
            if str(type(value)).split("'")[1] in ['str', 'list', 'dict', 'tuple', 'int', 'float', 'bool']:
                return str(type(value)).split("'")[1]
            return 'object'
    return 'string'

@register.filter
def entity_type(value):
    if value:
        if isinstance(value, dict):
            if value['key']:
                return value['key']
            else:
                from random import randint
                return 'dict-'+str(randint(100000, 10000000))

        if isinstance(value, list):
            value = value[0]
        if len(str(type(value)).split("'")) >= 2:
            return str(type(value)).split("'")[1].split('.')[-1]
    return type(value)

# TODO: is_instance filter
@register.filter
def is_instance(value, token):
    return isinstance(value, token)

@register.filter
def is_string(value):
    return isinstance(value, str)

@register.filter
def is_list(value):
    return isinstance(value, list)

@register.filter
def is_dict(value):
    return isinstance(value, dict)

@register.filter
def is_int(value):
    return isinstance(value, int)

@register.filter
def is_float(value):
    return isinstance(value, float)

@register.filter
def is_bool(value):
    return isinstance(value, bool)

#@stringfilter
@register.filter
def permalink(value):
    return re.sub('[-\s]+', '-', re.sub('[^\w\s-]', '', value.strip().lower()))

#
# Compressors
#
""" --------- """
#
# CSSMin
#
@register.tag
def include_cssmin(parser, token):
    try:
        tag_name, format_string = token.split_contents()
    except ValueError:
        raise TemplateSyntaxError, "%r tag requires a single argument" % token.contents.split()[0]
    if not (format_string[0] == format_string[-1] and format_string[0] in ('"', "'")):
        raise TemplateSyntaxError, "%r tag's argument should be in quotes" % tag_name
    return cssmin_Compressed(file=format_string[1:-1])

@register.tag
def compress_cssmin(parser, token):
    nodelist = parser.parse(('endcompress_cssmin',))
    parser.delete_first_token()
    return cssmin_Compressed(nodelist=nodelist)

class cssmin_Compressed(Node):
    def __init__(self, file=None, string=None, nodelist=None):
        self.file = file
        self.nodelist = nodelist

    def render(self, context):
        from cssmin import cssmin
        if self.file:
            for c in context:
                if c.has_key('site'):
                    import os
                    if os.path.exists(c['site'].template_path(self.file)):
                        return cssmin(template.render(c['site'].template_path(self.file), {}))
                    else:
                        raise TemplateSyntaxError, "CSS Stylesheet file (%r) not present: %r" % (self.file, c['site'].template_path(self.file))

        if self.nodelist:
            output = self.nodelist.render(context)
            return cssmin(output)

        raise TemplateSyntaxError, "Impossible to compress"

#
# CSSMin ENDS
#
#
# JSMin
#
@register.tag
def include_jsmin(parser, token):
    try:
        tag_name, format_string = token.split_contents()
    except ValueError:
        raise TemplateSyntaxError, "%r tag requires a single argument" % token.contents.split()[0]
    if not (format_string[0] == format_string[-1] and format_string[0] in ('"', "'")):
        raise TemplateSyntaxError, "%r tag's argument should be in quotes" % tag_name
    return jsmin_Compressed(file=format_string[1:-1])

@register.tag
def compress_jsmin(parser, token):
    nodelist = parser.parse(('endcompress_jsmin',))
    parser.delete_first_token()
    return jsmin_Compressed(nodelist=nodelist)

class jsmin_Compressed(Node):
    def __init__(self, file=None, string=None, nodelist=None):
        self.file = file
        self.nodelist = nodelist

    def render(self, context):
        from jsmin import jsmin
        if self.file:
            for c in context:
                if c.has_key('site'):
                    import os
                    if os.path.exists(c['site'].template_path(self.file)):
                        return jsmin(template.render(c['site'].template_path(self.file), {}))
                    else:
                        raise TemplateSyntaxError, "JavaScript file (%r) not present: %r" % (self.file, c['site'].template_path(self.file))

        if self.nodelist:
            output = self.nodelist.render(context)
            return jsmin(output)

        raise TemplateSyntaxError, "Impossible to compress"

#
# JSMin ENDS
#
""" ------------- """
#
# YuiCompressor
#
@register.tag
def include_yui(parser, token):
    try:
        tag_name, format_string = token.split_contents()
    except ValueError:
        raise TemplateSyntaxError, "%r tag requires a single argument" % token.contents.split()[0]
    if not (format_string[0] == format_string[-1] and format_string[0] in ('"', "'")):
        raise TemplateSyntaxError, "%r tag's argument should be in quotes" % tag_name
    return yui_Compressed(file=format_string[1:-1])

@register.tag
def compress_yui(parser, token):
    nodelist = parser.parse(('endcompress_yui',))
    parser.delete_first_token()
    return yui_Compressed(nodelist=nodelist)

class yui_Compressed(Node):
    def __init__(self, file=None, string=None, nodelist=None):
        self.file = file
        self.nodelist = nodelist

    def render(self, context):
        from yuicompressor import compress
        #from closurecompiler import compress
        if self.file:
            for c in context:
                if c.has_key('site'):
                    import os
                    if os.path.exists(c['site'].template_path(self.file)):
                        return compress(template.render(c['site'].template_path(self.file), {}))
                    else:
                        raise TemplateSyntaxError, "file (%r) not present: %r" % (self.file, c['site'].template_path(self.file))

        if self.nodelist:
            output = self.nodelist.render(context)
            return compress(output)

        raise TemplateSyntaxError, "Impossible to compress"
#
# YuiCompressor ENDS
#
""" ------------- """
#
# ClosureCompiler
#
# JS

@register.tag
def include_ccompiler_js(parser, token):
    try:
        tag_name, format_string = token.split_contents()
    except ValueError:
        raise TemplateSyntaxError, "%r tag requires a single argument" % token.contents.split()[0]
    if not (format_string[0] == format_string[-1] and format_string[0] in ('"', "'")):
        raise TemplateSyntaxError, "%r tag's argument should be in quotes" % tag_name
    return ccompiler_js_Compressed(file=format_string[1:-1])

@register.tag
def compress_ccompiler_js(parser, token):
    nodelist = parser.parse(('endcompress_ccompiler_js',))
    parser.delete_first_token()
    return ccompiler_js_Compressed(nodelist=nodelist)

class ccompiler_js_Compressed(Node):
    def __init__(self, file=None, string=None, nodelist=None):
        self.file = file
        self.nodelist = nodelist

    def render(self, context):
        from closurecompiler import compress
        if self.file:
            for c in context:
                if c.has_key('site'):
                    import os
                    if os.path.exists(c['site'].template_path(self.file)):
                        return compress(template.render(c['site'].template_path(self.file), {}))
                    else:
                        raise TemplateSyntaxError, "file (%r) not present: %r" % (self.file, c['site'].template_path(self.file))

        if self.nodelist:
            output = self.nodelist.render(context)
            return compress(output)

        raise TemplateSyntaxError, "Impossible to compress"

# CSS

@register.tag
def include_ccompiler_css(parser, token):
    try:
        tag_name, format_string = token.split_contents()
    except ValueError:
        raise TemplateSyntaxError, "%r tag requires a single argument" % token.contents.split()[0]
    if not (format_string[0] == format_string[-1] and format_string[0] in ('"', "'")):
        raise TemplateSyntaxError, "%r tag's argument should be in quotes" % tag_name
    return ccompiler_css_Compressed(file=format_string[1:-1])

@register.tag
def compress_ccompiler_css(parser, token):
    nodelist = parser.parse(('endcompress_ccompiler_js',))
    parser.delete_first_token()
    return ccompiler_css_Compressed(nodelist=nodelist)

class ccompiler_css_Compressed(Node):
    def __init__(self, file=None, string=None, nodelist=None):
        self.file = file
        self.nodelist = nodelist

    def render(self, context):
        from closurecompiler import compress
        if self.file:
            for c in context:
                if c.has_key('site'):
                    import os
                    if os.path.exists(c['site'].template_path(self.file)):
                        return compress(template.render(c['site'].template_path(self.file), {}))
                    else:
                        raise TemplateSyntaxError, "file (%r) not present: %r" % (self.file, c['site'].template_path(self.file))

        if self.nodelist:
            output = self.nodelist.render(context)
            return compress(output)

        raise TemplateSyntaxError, "Impossible to compress"
#
# ClosureCompiler ENDS
#

# TODO: category getters
# TODO: registry
# TODO: Entities exportation (json,yaml,xml,rss,atom,ini,csv)


# TODO: Creare la tag dump_context per il printout caruccio di tutte le variabili passate al template con un redirect del template per evitare il printout normale (usando sempre i vari formati disponibii)
#@register.tag
#def load_settings(parser, token):
#    # TODO: Usare gbkit.settings()
#    from application.models import settings
#    entities = settings.Settings.all().fetch(1000, 0)
#    nodelist = parser.parse(('endload_settings',))
#    parser.delete_first_token()
#    return EntitiesNode(entities, 'loaded_settings', nodelist)

@register.tag
def load_settings(parser, token):
    # TODO: Usare gbkit.settings()
    from application.models import settings
    entities = settings.Settings.all().fetch(1000, 0)
    nodelist = parser.parse(('endload_settings',))
    parser.delete_first_token()
    return EntitiesNode(entities, 'loaded_settings', nodelist)

@register.tag
def load_settings_value(parser, token):
    # TODO: Usare gbkit.settings()
    try:
        tag_name, arg = token.contents.split(None, 1)
    except ValueError:
        raise TemplateSyntaxError, "%r tag requires arguments: field" % tag_name
    m = re.search(r'(.*?)', arg)
    if not m:
        raise TemplateSyntaxError, "%r tag had invalid arguments" % tag_name

    if not (arg[0] == arg[-1] and arg[0] in ('"', "'")):
        raise TemplateSyntaxError, "%r tag's argument(field) should be in quotes %s " % (tag_name, arg)

    from application.models import settings
    nodelist = parser.parse(('endload_settings_value',))
    parser.delete_first_token()
    try:
        entity = settings.Settings.all().filter('field =', arg[1:-1]).fetch(1, 0)[0] or {}
        return EntityNode(entity, 'loaded_settings_value', nodelist)
    except:
        return EntityNode({}, 'loaded_settings_value', nodelist)

@register.tag
def load_pages(parser, token):
    from application.models import pages
    entities = pages.Page.all().filter('disabled =', False).fetch(1000, 0)
    nodelist = parser.parse(('endload_pages',))
    parser.delete_first_token()
    return EntitiesNode(entities, 'loaded_pages', nodelist)

@register.tag
def load_page(parser, token):
    try:
        tag_name, arg = token.contents.split(None, 1)
    except ValueError:
        raise TemplateSyntaxError, "%r tag requires arguments: key or permalink" % tag_name
    m = re.search(r'(.*?)', arg)
    if not m:
        raise TemplateSyntaxError, "%r tag had invalid arguments" % tag_name

    if not (arg[0] == arg[-1] and arg[0] in ('"', "'")):
        raise TemplateSyntaxError, "%r tag's argument(key or permalink) should be in quotes %s " % (tag_name, arg)

    from application.models import pages
    nodelist = parser.parse(('endload_page',))
    parser.delete_first_token()
    try:
        entity = pages.Page.all().filter('disabled =', False).filter('permalink =', arg[1:-1]).fetch(1, 0)[0] or pages.Page.get(arg[1:-1]) or {}
        return EntityNode(entity, 'loaded_page', nodelist)
    except:
        return EntityNode({}, 'loaded_page', nodelist)

# TODO:  load_page_by_category tag
@register.tag
def load_page_by_category(parser, token):
    try:
        tag_name, arg = token.contents.split(None, 1)
    except ValueError:
        raise TemplateSyntaxError, "%r tag requires arguments: category" % token.contents.split()[0]
    m = re.search(r'(.*?)', arg)
    if not m:
        raise TemplateSyntaxError, "%r tag had invalid arguments" % tag_name
    format_string = m.groups()
    if not (format_string[0] == format_string[-1] and format_string[0] in ('"', "'")):
        raise TemplateSyntaxError, "%r tag's argument(category) should be in quotes" % tag_name

    from application.models import pages
    entity = pages.Page.all().filter('disabled =', False).filter('category =', format_string).fetch(1, 0)[0] or {}
    nodelist = parser.parse(('endload_page_by_category',))
    parser.delete_first_token()
    return EntityNode(entity, 'loaded_page', nodelist)

# TODO:  load_pages_by_category tag
@register.tag
def load_pages_by_category(parser, token):
    try:
        tag_name, arg = token.contents.split(None, 1)
    except ValueError:
        raise TemplateSyntaxError, "%r tag requires arguments: category" % token.contents.split()[0]
    m = re.search(r'(.*?)', arg)
    if not m:
        raise TemplateSyntaxError, "%r tag had invalid arguments" % tag_name

    if not (arg[0] == arg[-1] and arg[0] in ('"', "'")):
        raise TemplateSyntaxError, "%r tag's argument(category) should be in quotes %s " % (tag_name, arg)

    from application.models import pages
    nodelist = parser.parse(('endload_pages_by_category',))
    parser.delete_first_token()
    try:
        cat = pages.PageCategory.all(keys_only=True).filter('disabled =', False).filter('permalink =', arg[1:-1]).fetch(1)[0]
        entities = pages.Page.all().filter('disabled =', False).filter('category =', cat).fetch(1000, 0)
        return EntitiesNode(entities, 'loaded_pages', nodelist)
    except:
        return EntitiesNode([], 'loaded_pages', nodelist)