"""The jsonpickle.tags module provides the custom tags
used for pickling and unpickling Python objects.

These tags are keys into the flattened dictionaries
created by the Pickler class.  The Unpickler uses
these custom key names to identify dictionaries
that need to be specially handled.
"""
from jsonpickle.compat import set

ID = 'py___id'
OBJECT = 'py___object'
TYPE = 'py___type'
REPR = 'py___repr'
REF = 'py___ref'
TUPLE = 'py___tuple'
SET = 'py___set'
SEQ = 'py___seq'
STATE = 'py___state'

# All reserved tag names
RESERVED = set([OBJECT, TYPE, REPR, REF, TUPLE, SET, SEQ, STATE])
