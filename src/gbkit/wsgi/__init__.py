#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# GBKit
# - WebApps Python Handy Framework
#
# Copyright 2012 GB Consulting
# Author: Filippo Baruffaldi <filippo@baruffaldi.info>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
"""
@author: Filippo Baruffaldi <filippo@baruffaldi.info>
@organization: GB Consulting
@project: GBKit
@license: Apache 2.0
"""
__author__ = "Filippo Baruffaldi <filippo@baruffaldi.info>"
__organization__ = "GB Consulting"
__project__ = "GBKit"
__license__ = "Apache 2.0"
__url__ = "http://www.gbc-italy.com"
__version__ = 1

import webapp2


class Request(webapp2.Request):
    def get(self, argument_name, default_value='', allow_multiple=False, type=False):
        """Returns the query or POST argument with the given name.

        We parse the query string and POST payload lazily, so this will be a
        slower operation on the first call.

        :param argument_name:
            The name of the query or POST argument.
        :param default_value:
            The value to return if the given argument is not present.
        :param allow_multiple:
            Return a list of values with the given name (deprecated).
        :returns:
            If allow_multiple is False (which it is by default), we return
            the first value with the given name given in the request. If it
            is True, we always return a list.
        """
        param_value = self.get_all(argument_name)
        #if allow_multiple:
        #    logging.warning('allow_multiple is a deprecated param. '
        #        'Please use the Request.get_all() method instead.')

        if len(param_value) > 0:
            if allow_multiple:
                return param_value

            return param_value[0] if not type else type(param_value[0])
        else:
            if allow_multiple and not default_value:
                return []

            return default_value


class WSGIApplication(webapp2.WSGIApplication):
    """Wraps a set of webapp RequestHandlers in a WSGI-compatible application.

    To use this class, pass a list of (URI regular expression, RequestHandler)
    pairs to the constructor, and pass the class instance to a WSGI handler.
    See the example in the module comments for details.

    The URL mapping is first-match based on the list ordering.
    """

    request_class = Request

    def __init__(self, routes, debug, config):
        """self.error_handlers[403] = ContentProhibited
        self.error_handlers[404] = ContentNotFound
        self.error_handlers[500] = ContentNotWorking"""
        return super(WSGIApplication, self).__init__(
            routes, debug, config)
