@echo off
echo GPRESS-UPLOADER: Clear Unused Indexes
appcfg.py vacuum_indexes C:\Users\bornslippy\workspace\gpress-professional\src
echo GPRESS-UPLOADER: Updating Indexes
appcfg.py update_indexes C:\Users\bornslippy\workspace\gpress-professional\src
pause