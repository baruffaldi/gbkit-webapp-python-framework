#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# GBKit
# - WebApps Python Handy Framework
#
# Copyright 2012 GB Consulting
# Author: Filippo Baruffaldi <filippo@baruffaldi.info>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
"""
@author: Filippo Baruffaldi <filippo@baruffaldi.info>
@organization: GB Consulting
@project: GBKit
@license: Apache 2.0
"""
__author__ = "Filippo Baruffaldi <filippo@baruffaldi.info>"
__organization__ = "GB Consulting"
__project__ = "GBKit"
__license__ = "Apache 2.0"
__url__ = "http://www.gbc-italy.com"
__version__ = 1

from google.appengine.ext import db
from gbkit.wsgi import WSGIApplication
from gbkit.wsgi.handlers import CRUDAPIRequestHandler, RESTCRUDAPIRequestHandler, DEBUG, wsgi_config


class Get(CRUDAPIRequestHandler):
    def post(self, key=None):
        model = self._crud_api_request['request']['_handler']
        if key is None:
            key = self.request.get('key', default_value=None)
        try:
            entry = model.get(key)
        except:
            self.error(404)
            return
        self.render(None, entry)


class List(CRUDAPIRequestHandler):
    def post(self):
        model = self._crud_api_request['request']['_handler']
        entries = model.all().fetch(
            self._crud_api_request['limit'],
            self._crud_api_request['start'])
        self.render(None, entries)


class Update(CRUDAPIRequestHandler):
    def post(self, action=None):
        model = self._crud_api_request['request']['_handler']
        if action in ['update', 'insert', 'new']:
            self.raise_error('Datastore API Method not found', 405)

        entity_parameters = {}
        for field in self.request_vars().keys():
            if self.request.get(field, default_value=False) and \
                field not in model._protected and \
                field not in model._collections and \
                field not in model._collections_single_reference:
                entity_parameters[field] = self.request.get(field)

        if action == 'update' and self.request.get('key', default_value=False):
            entry = model.get(self.request.get('key'))
            entry.intelligent_update(entity_parameters)
        else:
            entry = model(**entity_parameters)
        entry.save()
        self.render(None, entry)


class Delete(CRUDAPIRequestHandler):
    def post(self):
        entries_to_delete = self.request.get_all('key')
        db.delete([db.Key(x) for x in entries_to_delete])
        self.render(None, True)


class Rest(RESTCRUDAPIRequestHandler):
    def post(self, key=None):
        model = self._crud_api_request['request']['_handler']
        entity_parameters = {}
        for field in self.request_vars().keys():
            if self.request.get(field, default_value=False) and \
                field not in model._protected and \
                field not in model._collections and \
                field not in model._collections_single_reference:
                entity_parameters[field] = self.request.get(field)

        try:
            entry = model(**entity_parameters)
        except:
            self.raise_error('Entity not found.', 404)
        if entry is not None:
            entry.intelligent_update(entity_parameters)
            entry.save()
        self.render(None, entry)

    def get(self, key=None):
        model = self._crud_api_request['request']['_handler']
        if key is None:
            key = self.request.get('key', default_value=None)
        try:
            entry = model.get(key)
        except:
            self.error(404)
            return
        self.render(None, entry)

    def put(self, key=None):
        model = self._crud_api_request['request']['_handler']
        entity_parameters = {}
        for field in self.request_vars().keys():
            if self.request.get(field, default_value=False) and \
                field not in model._protected and \
                field not in model._collections and \
                field not in model._collections_single_reference:
                entity_parameters[field] = self.request.get(field)

        if key is None:
            key = self.request.get('key', default_value=None)
        if key is not None:
            entry = None
            try:
                entry = model.get(key)
            except:
                self.raise_error('Entity not found.', 404)
            if entry is not None:
                entry.intelligent_update(entity_parameters)
                entry.save()
        else:
            self.raise_error('Entity not specified.', 404)
        self.render(None, entry)

    def delete(self, key=None):
        entries_to_delete = key.split('/')
        db.delete(entries_to_delete)
        self.render(None, True)


class NotFound(CRUDAPIRequestHandler):
    def get(self):
        self.raise_error('Datastore API Method not found', 405)


routes = [
    (r'/gbkit/crud/datastore/.*/list', List),
    (r'/gbkit/crud/datastore/.*/(update|insert|new|create)', Update),
    (r'/gbkit/crud/datastore/.*/delete', Delete),
    (r'/gbkit/crud/datastore/.*/get/(.*)', Get),
    (r'/gbkit/crud/datastore/.*/get/', Get),
    (r'/gbkit/crud/datastore/.*/get', Get),
    (r'/gbkit/crud/datastore/.*/read/(.*)', Get),
    (r'/gbkit/crud/datastore/.*/read/', Get),
    (r'/gbkit/crud/datastore/.*/read', Get),
    (r'/gbkit/crud/datastore/.*/retrieve/(.*)', Get),
    (r'/gbkit/crud/datastore/.*/retrieve/', Get),
    (r'/gbkit/crud/datastore/.*/retrieve', Get),
    (r'/gbkit/rest/datastore/.*/(.*)', Rest),
    (r'/gbkit/rest/datastore/.*/', Rest),
    (r'.*', NotFound)
]
app = WSGIApplication(routes, debug=DEBUG, config=wsgi_config)
