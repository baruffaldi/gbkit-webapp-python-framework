/**
 * GBKit
 * - WebApps Python Handy Framework
 *
 * Copyright 2012 GB Consulting
 * Author: Filippo Baruffaldi <filippo@baruffaldi.info>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/**
 * @author: Filippo Baruffaldi <filippo@baruffaldi.info>
 * @organization: GB Consulting
 * @project: GBKit
 * @license: Apache 2.0
 */

var GBKit = {}

GBKit.Strings = {}

GBKit.Strings.Trim = function (s){
  return s.replace(/^\s\s*/, '').replace(/\s\s*$/, '');
}

GBKit.IncludedResources = function() {
	return {'TinyMCE': false}
}

GBKit.GenerateToken = function () {
	this.current_token = Math.random();
	return Math.random();
}

GBKit.PrintToken = function () {
	if ( typeof this.current_token == "undefined" )
    this.GenerateToken();
	alert(this.current_token);
}

GBKit.GetXHR = function () {
  if (typeof XMLHttpRequest != "undefined") {
    return new XMLHttpRequest();
  }
  try { return new ActiveXObject("Msxml2.XMLHTTP.6.0") } catch(e) {}
  try { return new ActiveXObject("Msxml2.XMLHTTP.3.0") } catch(e) {}
  try { return new ActiveXObject("Msxml2.XMLHTTP") } catch(e) {}
  try { return new ActiveXObject("Microsoft.XMLHTTP") } catch(e) {}
  return null;
}

GBKit.IncludeScript = function (script, text) {
if (document.createElement && document.getElementsByTagName) {
     var ScriptElement = document.createElement('script');
     ScriptElement.setAttribute('type', 'text/javascript');

     if ( ! GBKit.Empty(script) )
      ScriptElement.setAttribute('src', script);

     if ( ! GBKit.Empty(text) )
      ScriptElement.innerHTML = text;

     var head = document.getElementsByTagName('head')[0];
     head.appendChild(ScriptElement);
     return ScriptElement;
 }
}
//GBKit.IncludeScript('http://tinymce.moxiecode.com/js/tinymce/jscripts/tiny_mce/tiny_mce.js');
GBKit.Bootstrap = function () {
  /* Detect textareas
   */
  Event.observe(window, 'load', function () {
    if ( ( $$('textarea[class~="mceEditor"]').length || $$('textarea[class~="mceRichText"]').length || $$('textarea[class~="mceLightEditor"]').length ) && ! GBKit.IncludedResources().TinyMCE )
    {
        GBKit.IncludedResources.TinyMCE = true;
        GBKit.TinyMCE();
    }

    Tables = $$('table[class~="gbkit-admin-table-list"]')
    if ( Tables.length )
      GBKit.ProcessTables(Tables);

    ElementsToHide = $$('[class~="javascript_hide"]');
    ElementsToHide.each(function(element){
      element.setStyle({display:'none'});
    });

    Tabs = $$('[class~="tabs"]');
    Tabs.each(function(element){
      new Control.Tabs(element);
    });


																					var tabs_example_two = new Control.Tabs('tabs_example_two',{
																					    afterChange: function(new_container){
																					        $A($('tabs_example_two_select').options).each(function(option,i){
																					            if(option.value == new_container.id){
																					                $('tabs_example_two_select').options.selectedIndex = i;
																					                throw $break;
																					            }
																					        });
																					    }
																					});
																					$('tabs_example_two_select').observe('change',function(){
																					    tabs_example_two.setActiveTab($('tabs_example_two_select').value);
																					});
																					$('tabs_example_two_first').observe('click',function(event){
																					    this.first();
																					    Event.stop(event);
																					}.bindAsEventListener(tabs_example_two));
																					$('tabs_example_two_previous').observe('click',function(event){
																					    this.previous();
																					    Event.stop(event);
																					}.bindAsEventListener(tabs_example_two));
																					$('tabs_example_two_next').observe('click',function(event){
																					    this.next();
																					    Event.stop(event);
																					}.bindAsEventListener(tabs_example_two));
																					$('tabs_example_two_last').observe('click',function(event){
																					    this.last();
																					    Event.stop(event);
																					}.bindAsEventListener(tabs_example_two));

  });
}

GBKit.TinyMCE = function () {
	tinyMCE.init({
		mode : "exact",
      mode : "specific_textareas",
      editor_selector : /(mceEditor|mceRichText)/,
		theme : "advanced",
		skin : "o2k7",
		skin_variant : "black",
		plugins : "safari,spellchecker,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,imagemanager,filemanager",
		theme_advanced_buttons1 : "insertlayer,moveforward,movebackward,absolute,|,styleprops,spellchecker,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,template,blockquote,pagebreak,|,insertfile,insertimage",
		theme_advanced_buttons2 : "tablecontrols,|,hr,removeformat,visualaid,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote",
		theme_advanced_buttons3 : "cut,copy,paste,pastetext,pasteword,|,undo,redo,|,sub,sup,|,charmap,emotions,iespell,media,advhr,|,print,|,ltr,rtl,|,fullscreen,|,insertdate,inserttime,preview",
		theme_advanced_buttons4 : "bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,formatselect,fontsizeselect,link,unlink,anchor,image,cleanup,help,code",
		theme_advanced_toolbar_location : "top",
		theme_advanced_toolbar_align : "left",
		theme_advanced_statusbar_location : "bottom",
		theme_advanced_resizing : true,
		template_replace_values : {
		username : "Some User",
		staffid : "991234"
}
});

  tinyMCE.init({
    mode : "exact",
      mode : "specific_textareas",
      editor_selector : /(mceLightEditor)/,
    theme : "simple",
    skin : "o2k7",
    skin_variant : "black",
    theme_advanced_toolbar_location : "top",
    theme_advanced_toolbar_align : "left",
    theme_advanced_statusbar_location : "bottom",
    theme_advanced_resizing : true
});/*
  tinyMCE.init({
      //mode : "textareas",
      mode : "specific_textareas",
      editor_selector : /(mceEditor|mceRichText)/,
     theme : "advanced",
      skin : "o2k7",
     skin_variant : "black",
     plugins : "safari,spellchecker,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,imagemanager,filemanager",theme_advanced_buttons1 : "save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,styleselect,formatselect,fontsizeselect",
      theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code,|,insertdate,inserttime,preview,|,forecolor,backcolor",
      theme_advanced_buttons3 : "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr,|,print,|,ltr,rtl,|,fullscreen",
      theme_advanced_buttons4 : "insertlayer,moveforward,movebackward,absolute,|,styleprops,spellchecker,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,template,blockquote,pagebreak,|,insertfile,insertimage",
      theme_advanced_toolbar_location : "top",
      theme_advanced_toolbar_align : "left",
      theme_advanced_statusbar_location : "bottom",
      theme_advanced_resizing : true,
      template_external_list_url : "http://tinymce.moxiecode.com/js/template_list.js",
      external_link_list_url : "http://tinymce.moxiecode.com/js/link_list.js",
      external_image_list_url : "http://tinymce.moxiecode.com/js/image_list.js",
      media_external_list_url : "http://tinymce.moxiecode.com/js/media_list.js"
       });*/
}

GBKit.ProcessTables = function (tables) {
  SelectAllCheckBox = $$('th[class~="select_all-box"]');
  SelectAllCheckBox.each(function(item){
    item.innerHTML = '<input class="select_all" type="checkbox" name="'+item.id+'" value="1" />';
  });
  var statuses = {};
  SelectAllCheckBox = $$('input[class~="select_all"]');
  SelectAllCheckBox.each(function(item){
    statuses[item.name] = false;
    item.checked = status;
    item.observe('click', function(i){
      if ( statuses[item.name] ) statuses[item.name] = false;
      else statuses[item.name] = true;
      AllCheckBox = $$('input[class~="'+item.name+'"]');
      AllCheckBox.each(function(i){
        i.checked = statuses[item.name];
        SwitchRowColor(i.parentNode.parentNode);
      });
      SelectAllCheckBox.each(function(it){
      	it.checked = statuses[item.name];
      });
    });
  });


  GetRowCheckBox = function(row){
    return row.childNodes[1].firstChild;
  }

  SwitchRow = function(row){
      cb = GetRowCheckBox(row)
      if ( cb.checked )
        cb.checked = false;
      else
        cb.checked = true;
  }

  SwitchRowColor = function(row){
      cb = GetRowCheckBox(row)
      if ( cb.checked ) {
        row.addClassName('gbkit-admin-table-active-row');
        row.removeClassName(row.name);
      } else {
        row.addClassName(row.name);
        row.removeClassName('gbkit-admin-table-active-row');
      }
  }

  ProcessTableRows = function (rows){
    rows.each(function(item){
      item.name = item.className;
      id = item.id;
      key = id.substring(4);
      item.observe('click', function(){
        SwitchRow(item);
        SwitchRowColor(item);
      });
      item.observe('dblclick', function(){
        window.location.href = document.getElementById('link_'+key).href;
      });
      cb = GetRowCheckBox(item);
      cb.setStyle({display: 'none'});
      cb.parentNode.setStyle({width: '10px'});
    });
  }

  TableRows = $$('tr[class~="gbkit-admin-table-row"]');
  TableAltRows = $$('tr[class~="gbkit-admin-table-alt-row"]');
  ProcessTableRows(TableRows);
  ProcessTableRows(TableAltRows);
}

GBKit.Empty = function (v) {
      return (typeof v == 'undefined' || v == '' || v == null);
}

/*
Elements = $$('[class~="javascript_hide_controller"]');

GBKit.Controllers = {}
GBKit.Controllers.HideElements = function (elements, type='image'|'shadow') {
    elements.each(function(el) {
        var Id = el.id;
        if ( ! GBKit.Empty(Id) )
            Id = el.href.substring(1);
        var ElementToHide = $('javascript_hide_'+Id);
        ElementToHide.setStyle({display:'none'});
        Event.observe(el, 'click', function() {
            if (el.style.display == 'none')
                el.setStyle({display:'none'});
            else
                el.setStyle({display:''});
        }
    }
}
GBKit.Controllers.HideElements(Elements);

Elements = $$('img[class~="gbkit-admin-table-list"]')
GBKit.HighlightElements(Elements);

GBKit.HighlightElements = function (images, type='image'|'shadow') {
* Check for element type ( se immagine usare src )
    images.each(function(image) {
        var image_filename = image.src.split('.');
        var image_name = image_filename[:1];
        var image_ext = image_filename[-1];
        Event.observe(image, 'hover', function () {
            img.src = image_name + '-active.' + image_ext;
        });
        Event.observe(image, 'out', function () {
            img.src = image_name + image_ext;
        });
    });
}*/


GP = GBKit
