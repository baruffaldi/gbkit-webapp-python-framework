#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# GBKit
# - WebApps Python Handy Framework
#
# Copyright 2012 GB Consulting
# Author: Filippo Baruffaldi <filippo@baruffaldi.info>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
"""
@author: Filippo Baruffaldi <filippo@baruffaldi.info>
@organization: GB Consulting
@project: GBKit
@license: Apache 2.0
"""
__author__ = "Filippo Baruffaldi <filippo@baruffaldi.info>"
__organization__ = "GB Consulting"
__project__ = "GBKit"
__license__ = "Apache 2.0"
__url__ = "http://www.gbc-italy.com"
__version__ = 1

from UserDict import *


class Object(IterableUserDict):
    data = {}

    def __setitem__(self, key, value):
        super(IterableUserDict, self).__setitem__(self, key, value)

    def __getitem__(self, key, default=None):
        super(IterableUserDict, self).__getitem__(self, key, default)

    def update(self, *args, **kwargs):
        if len(args) > 1:
            raise TypeError("update expected at most 1 arguments, got %d"\
                % len(args))
        other = dict(*args, **kwargs)
        for key in other:
            self[key] = other[key]
