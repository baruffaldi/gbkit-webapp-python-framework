#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# GBKit
# - WebApps Python Handy Framework
#
# Copyright 2012 GB Consulting
# Author: Filippo Baruffaldi <filippo@baruffaldi.info>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
"""
@author: Filippo Baruffaldi <filippo@baruffaldi.info>
@organization: GB Consulting
@project: GBKit
@license: Apache 2.0
"""
__author__ = "Filippo Baruffaldi <filippo@baruffaldi.info>"
__organization__ = "GB Consulting"
__project__ = "GBKit"
__license__ = "Apache 2.0"
__url__ = "http://www.gbc-italy.com"
__version__ = 1

from gbkit.wsgi import WSGIApplication
from gbkit.wsgi.handlers import RequestHandler, DEBUG, wsgi_config


class TemplateAutoRender(RequestHandler):
    def get(self, template_path=None):
        self.render(self.get_template(template_path))


class PlaceHolder(RequestHandler):
    def get(self, *args, **kwargs):
        self.raise_error(code=404)


routes = [
    #(r'(.*)', TemplateAutoRender),
    (r'(.*)', PlaceHolder)
]
app = WSGIApplication(routes, debug=DEBUG, config=wsgi_config)
