#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# GBKit
# - WebApps Python Handy Framework
#
# Copyright 2012 GB Consulting
# Author: Filippo Baruffaldi <filippo@baruffaldi.info>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
"""
@author: Filippo Baruffaldi <filippo@baruffaldi.info>
@organization: GB Consulting
@project: GBKit
@license: Apache 2.0
"""
__author__ = "Filippo Baruffaldi <filippo@baruffaldi.info>"
__organization__ = "GB Consulting"
__project__ = "GBKit"
__license__ = "Apache 2.0"
__url__ = "http://www.gbc-italy.com"
__version__ = 1

import sys
import os
import django

VERSION_CORE = os.environ['GBKIT_VERSION']
VERSION_PYTHON = sys.version
VERSION_SDK = os.environ.get('SDK_VERSION', 'latest')
VERSION_DJANGO = django.get_version()
VERSION_APPLICATION = os.environ.get('CURRENT_VERSION_ID', 'latest')


def versions():
    return {
        'python': VERSION_PYTHON,
        'sdk': VERSION_SDK,
        'django': VERSION_DJANGO,
        'application': VERSION_APPLICATION,
        'core': VERSION_CORE,
    }

PACKAGE_NAME = "GBKit - v. %s (+http://repository.gbconsulting.it/gbkit-webapp-python-framework)" \
    % os.environ['GBKIT_VERSION']

SECRET_KEY = "NobodyCanHackMyMind"

OPENID_PROVIDERS = {
    'GMail (Google)': ['Google.com/accounts/o8/id', 'Google.com/accounts/o8/id'],
#    'Yahoo': ['Yahoo.com', ''],
#    'MyOpenID': ['MyOpenID.com',''],
    'Blogger': ['Blogger.com', '%s.blogger.com'],
    'Bloglines': ['Bloglines.com', 'd.bloglines.com/%s'],
    'Flickr': ['Flickr.com', 'flickr.com/photos/%s'],
    'LiveDoor': ['LiveDoor.com', 'profile.livedoor.com/%s'],
    'MyBlogLog': ['MyBlogLog.com', 'mybloglog.com/buzz/memebers/%s'],
    'MySpace': ['MySpace.com', 'myspace.com/%s'],
    'Orange France': ['', '%s.orange.fr'],
    'SmugMug': ['SmugMug.com', '%s.smugmug.com'],
    'Technorati': ['Technorati.com', 'technorati.com/people/technorati/%s'],
    'WordPress': ['WordPress.com', '%s.wordpress.com'],
    'Ziki': ['Ziki.com', 'my.ziki.com/%s']
}

ENABLED_FORMAT = [
    "html",  # "htmlp"
    "json", "jsonp", "htmljsonp",
    # "xml", "rss", "atom"
    "yaml",  # "csv", "ini"
    "pickle", "ubjson"
]

CAPTCHA_PRIVATE_KEY = ''
CAPTCHA_PUBLIC_KEY = ''
AKISMET_PUBLIC_KEY = ''
#CAPTCHA_PRIVATE_KEY = '6LeY5cwSAAAAAMweNbEcCFix1pKk9RFDlfRWzEcm'
#CAPTCHA_PUBLIC_KEY = '6LeY5cwSAAAAAGb0zrzGz65gnrhRgdDRW-W6-VwZ'
#AKISMET_PUBLIC_KEY = '9a318470dfd9'

SETTINGS_DEFAULT = {
    'meta_title': 'GBKit - WebApp Python Framework',
    'meta_subtitle': 'Trying to improve your web experience'
}

SETTINGS_EXCLUSIONS_PREFIXES = [
    'meta', 'robots'
]

ENVIRONMENT_DEBUG = os.environ.get('SERVER_SOFTWARE').startswith("Dev")

APPENGINE_SERVICES = [
    "blobstore", "datastore_v3", "images", "mail", "memcache",
    "taskqueue", "urlfetch", "xmpp"
]

APPENGINE_SERVICES_CAPABILITIES = {
    "datastore_v3": ["write"]
}

HTTP_STATUSES = {
    100: 'Continue',
    101: 'Switching Protocols',
    200: 'OK',
    201: 'Created',
    202: 'Accepted',
    203: 'Non-Authoritative Information',
    204: 'No Content',
    205: 'Reset Content',
    206: 'Partial Content',
    300: 'Multiple Choices',
    301: 'Moved Permanently',
    302: 'Moved Temporarily',
    303: 'See Other',
    304: 'Not Modified',
    305: 'Use Proxy',
    306: 'Unused',
    307: 'Temporary Redirect',
    400: 'Bad Request',
    401: 'Unauthorized',
    402: 'Payment Required',
    403: 'Forbidden',
    404: 'Not Found',
    405: 'Method Not Allowed',
    406: 'Not Acceptable',
    407: 'Proxy Authentication Required',
    408: 'Request Time-out',
    409: 'Conflict',
    410: 'Gone',
    411: 'Length Required',
    412: 'Precondition Failed',
    413: 'Request Entity Too Large',
    414: 'Request-URI Too Large',
    415: 'Unsupported Media Type',
    416: 'Requested Range Not Satisfiable',
    417: 'Expectation Failed',
    500: 'Internal Server Error',
    501: 'Not Implemented',
    502: 'Bad Gateway',
    503: 'Service Unavailable',
    504: 'Gateway Time-out',
    505: 'HTTP Version not supported'
}

BASEPATH = os.path.abspath(os.path.split(__file__)[0])
