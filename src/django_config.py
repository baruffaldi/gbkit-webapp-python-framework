#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# GBKit
# - WebApps Python Handy Framework
#
# Copyright 2012 GB Consulting
# Author: Filippo Baruffaldi <filippo@baruffaldi.info>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
"""
@author: Filippo Baruffaldi <filippo@baruffaldi.info>
@organization: GB Consulting
@project: GBKit
@license: Apache 2.0
"""
__author__ = "Filippo Baruffaldi <filippo@baruffaldi.info>"
__organization__ = "GB Consulting"
__project__ = "GBKit"
__license__ = "Apache 2.0"
__url__ = "http://www.gbc-italy.com"
__version__ = 1

import os
import gbkit_config
import django_config
import django.conf
if not django.conf.settings.configured:
    django.conf.settings.configure(
        django_config, DEBUG=gbkit_config.ENVIRONMENT_DEBUG)
os.environ["DJANGO_SETTINGS_MODULE"] = 'django_config'

SERVER_SOFTWARE = os.environ.get('SERVER_SOFTWARE', '')
DEV_APPSERVER = gbkit_config.ENVIRONMENT_DEBUG
# These variables available in all template files using RequestContext
SAFE_SETTINGS = [
    'APPLICATION_ID', 'CURRENT_VERSION_ID',
    'SERVER_SOFTWARE', 'AUTH_DOMAIN', 'ANALYTICS_CODE',
    'SITE_NAME', 'SITE_OWNER', 'SITE_CONTACT_INFO', 'SITE_EMAIL_FROM',
    'DEV_APPSERVER', 'DEBUG', 'TEMPLATE_DEBUG',
    'ADMINS', 'MANAGERS',
    'DEFAULT_DOMAIN', 'WWW_DOMAIN', 'DOMAINS',
    'MEDIA_URL', 'MEDIA_VERSION', 'LIB_URL', 'LIB_VERSION',
    'COMBINE_FILES',
]

"""
    Configuration Begins here
"""
SERVER_EMAIL = 'dev@gbc-italy.com'
ADMINS = (
    ('Filippo Baruffaldi', 'dev@gbc-italy.com'),
)
SITE_NAME = "GBKit WebApp Python Framework"
SITE_OWNER = "GB Consulting"
SITE_CONTACT_INFO = "www.gbc-italy.com"
SITE_EMAIL_FROM = "support@pageforest.com"

"""
ANALYTICS_CODE = "UA-2072869-2"
DEFAULT_DOMAIN = 'coolminds.org'
if DEV_APPSERVER:
    WWW_DOMAIN = 'local:8084'
else:
    WWW_DOMAIN = 'www.' + DEFAULT_DOMAIN

DOMAINS = [
    'coolminds.org',
    'demo-gbkit.appspot.com',
    'localhost',
]"""

"""
    Configuration Ends here
"""

PROJECT_ROOT = os.path.normpath(os.path.dirname(__file__))
TEMPLATE_DIRS = (
    os.path.join(PROJECT_ROOT, "application", "views", "shared"),
    os.path.join(PROJECT_ROOT, "application", "views"),
    os.path.join(PROJECT_ROOT, "gbkit", "views", "shared"),
    os.path.join(PROJECT_ROOT, "gbkit", "views"),
    PROJECT_ROOT,)

TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.load_template_source',
    'django.template.loaders.app_directories.load_template_source',
)

INSTALLED_APPS = ('application',)
    #'djangoappengine',)

FILE_CHARSET = 'utf-8'
TEMPLATE_STRING_IF_INVALID = ''  # GBKit Unicode Error'

MIDDLEWARE_CLASSES = [
]

# App Engine specific environment variables.
APPLICATION_ID = os.environ.get('APPLICATION_ID', '')
CURRENT_VERSION_ID = os.environ.get('CURRENT_VERSION_ID', '')
SERVER_SOFTWARE = os.environ.get('SERVER_SOFTWARE', '')
AUTH_DOMAIN = os.environ.get('AUTH_DOMAIN', '')

RUNNING_ON_GAE = SERVER_SOFTWARE.startswith("Google App Engine")

# Disable it if you have too much load
USE_APPSTATS = not DEV_APPSERVER

MANAGERS = ADMINS
# Memcache prefix for Channel API caches
CHANNEL_PREFIX = 'CH1'
TIME_ZONE = 'UTC'
# If you set this to False, Django will make some optimizations so as not
# to load the internationalization machinery.
USE_I18N = False

# We don't let CommonMiddleware create ETag headers because we
# decorate our view functions with @last_modified instead.
USE_ETAGS = False

# We don't let CommonMiddleware redirect to add www or slashes.
PREPEND_WWW = False
APPEND_SLASH = False

# Content-Type header for JSON data.
JSON_MIMETYPE = 'application/json'
JSON_MIMETYPE_CS = JSON_MIMETYPE + '; charset=utf-8'

# Allowed formats for pageforest identifiers:
USERNAME_REGEX = r"[a-zA-Z][a-zA-Z0-9-]{,30}[a-zA-Z0-9]"
APP_ID_REGEX = r"[a-z][a-z0-9-]{,30}[a-z0-9]"
DOC_ID_REGEX = r"[a-zA-Z0-9_][a-zA-Z0-9\._-]{,99}"
TAG_REGEX = r"([a-z0-9]{1,20}:)?" + DOC_ID_REGEX

ADMIN_SUBDOMAIN = 'admin'

# Absolute path to the directory that holds media.
# Example: "/home/media/media.lawrence.com/"
MEDIA_ROOT = os.path.join(PROJECT_ROOT, 'static')

# URL that handles the media served from MEDIA_ROOT. Make sure to use a
# trailing slash if there is a path component (optional in other cases).
# Examples: "http://media.lawrence.com", "http://example.com/media/"
MEDIA_URL = '/asset/'
MEDIA_VERSION = '2'

SECRET_KEY = 'gbkitsy(#_hoi=$4&g%@a(azd+p%d1835z1pw@mxel+1ab%&^jlnq#@'

# Prevent account registration with some well-known usernames.
# This must be all lowercase, because it is matched against username.lower().
RESERVED_USERNAMES = """
public authenticated

admin administrator
staff info spam abuse
www www-data webmaster postmaster
test tester testuser testclient
friends family
anonymous unknown noname
everybody anybody nobody private owner
""".split()
if USE_APPSTATS:
    MIDDLEWARE_CLASSES.insert(0,
        'google.appengine.ext.appstats.recording.AppStatsDjangoMiddleware')
